<?php
    session_start();
?>

<!DOCTYPE HTML>
<html>
    <head>
    </head>

    <body>
        <?php
            $dbconn = pg_connect("host=localhost
                                  port=5432
                                  dbname=mosb
                                  user=postgres
                                  password=password")
                      or die ("Could not connect: " . preg_last_error());
            $query = "SELECT * FROM Progetto WHERE username = $1 ORDER BY data DESC";
            $result = pg_query_params($dbconn, $query, array($_SESSION['username']));
            
            while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {

                echo "<div class='ProgettoPersonale'>";
                echo "<form name='formModificaRimuoviProgetto' action='modificaRimuoviProgetto.php' method='POST' onSubmit='return confermaRimozione();'>";

                echo "<br/>";
                echo "<input type='text' name='username' value='" . $line['username'] . "' hidden><br/>";
                echo "E-mail : &nbsp;&nbsp;&nbsp; <input type='email' name='email' value='" . $line['email'] . "' readonly><br/>";
                echo "Titolo : &nbsp;&nbsp;&nbsp;&nbsp; <input class='TitoloProgetto' type='text' name='titolo' value='" . $line['titolo'] . "' readonly><br/>";
                echo "Sezione : <input type='text' name='sezione' value='" . $line['sezione'] . "' readonly><br/>";
                echo "Descrizione : <br/><textarea name='descrizione' cols=60 rows=4 wrap='physical' readonly>" . $line['descrizione'] . "</textarea><br/>";
                echo "GPL : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type='text' class='text-center' name='gpl' value='" . $line['gpl'] . "' readonly><br/>";
                echo "Data : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type='text' name='data' value='" . $line['data'] . "' readonly><br/><br/>";
                echo "<span name='modifica'> Modifica </span>";
                echo "<input type='submit' name='inviaRimozione' value='Rimuovi'>";
                echo "<input type='submit' name='inviaModifica' value=' Invia Modifica' disabled>";
                echo "<br/><br/>";
                
                echo "</form>";
                
                echo "</div>";
            }
            
            pg_free_result($result);
            pg_close($dbconn);
        ?>
    </body>
</html>  