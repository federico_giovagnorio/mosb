<?php
    session_start();
?>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width-device-width" initial-scale="1">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="css/mosb.css"/>
        <script src="/css/bootstrap.min.js"></script>
    </head>

    <body class="text-center">
        <div class="navbar"></div>
        <?php
            $dbconn = pg_connect("host=localhost
                                port=5432
                                dbname=mosb
                                user=postgres
                                password=password")
                                or die ("Could not connect: " . pg_last_error());
            if (!(isset($_POST['inviaRegistrazione']))){
                header("Location: ../index.html");
            }

            $username=$_POST['userRegistrazione'];
            $email=$_POST['emailRegistrazione'];
            $password=$_POST['passwordRegistrazione'];

            $query1 = "SELECT * FROM utente WHERE username=$1";
            $ret1 = pg_query_params($dbconn, $query1, array($username));
            if ($line1=pg_fetch_array($ret1, null, PGSQL_ASSOC)){
                echo "<h1> Registrazione NON riuscita ! </h1>";
                echo "Utente già inserito, <a href='registrazione.html'>riprova</a>";
            }
            else {
                $query2 = "SELECT * FROM utente WHERE email=$1";
                $ret2 = pg_query_params($dbconn, $query2, array($email));
                if ($line2=pg_fetch_array($ret2, null, PGSQL_ASSOC)){
                    echo "<h1> Registrazione NON riuscita ! </h1>";
                    echo "Email già inserita, <a href='registrazione.html'>riprova</a>";
                }
                else {
                    $query3 = "INSERT INTO Utente values ($1, $2, $3)";
                    $ret3 = pg_query_params($dbconn, $query3, array($username, $email, md5($password)));
                    if ($ret3){
                        echo "<h1> Registrazione effettuata con successo !</h1>";
                        $_SESSION['username'] = $username;
                        echo "<div class='Intestazione'> Puoi tornare nella <a href='index.html'>HOME</a>, oppure puoi controllare i <a href='listaProgettoPersonale.html'>TUOI PROGETTI</a> </div>";
                    }
                    else {
                        echo "<h1> Registrazione NON riuscita! </h1>";
                        echo "Errore sconosciuto, <a href='registrazione.html'>riprova</a>";
                    }
                }
            }
                    
        ?>
    </body>
</html>
            
                
