CREATE TABLE Utente (
	username VARCHAR(50),
	email 	 VARCHAR(50) NOT NULL,
	password VARCHAR(256) NOT NULL,
	PRIMARY KEY(username),
	UNIQUE(email)
);

CREATE TABLE Progetto (
	username VARCHAR(50),
	email VARCHAR(50),
	titolo VARCHAR(30),
	sezione VARCHAR(30),
	descrizione VARCHAR(300),
	gpl BOOLEAN,
	data DATE,
	PRIMARY KEY(username, titolo),
	FOREIGN KEY (username) REFERENCES utente(username)
);