<!DOCTYPE HTML>
<html>
    <head></head>

    <body>
        <?php
            $dbconn = pg_connect("host=localhost
                                  port=5432
                                  dbname=mosb
                                  user=postgres
                                  password=password")
                      or die ("Could not connect: " . preg_last_error());
            $query = 'SELECT * FROM progetto ORDER BY data DESC';
            $result = pg_query($query) or die ("Query failed: " . preg_last_error());
            while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {
                echo "<div class='Progetto'>";
                
                echo "<br/>";
                echo "User : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='text' name='username' value='" . $line['username'] . "' readonly><br/>";
                echo "E-mail : &nbsp;&nbsp;&nbsp;<input type='email' name='email' value='" . $line['email'] . "' readonly><br/>";
                echo "Titolo : &nbsp;&nbsp;&nbsp;&nbsp;<input type='text' name='titolo' value='" . $line['titolo'] . "' readonly><br/>";
                echo "Sezione : <input class='SezioneProgetto' type='text' name='sezione' value='" . $line['sezione'] . "' readonly><br/>";
                echo "Descrizione : <br/><textarea name='descrizioneProgetto' cols=60 rows=4 wrap='physical' readonly>" . $line['descrizione'] . "</textarea><br/>";
                echo "GPL : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class='GplProgetto text-center' type='text' name='gpl' value='" . $line['gpl'] . "' readonly><br/>";
                echo "Data : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='text' name='data' value='" . $line['data'] . "' readonly><br/>";

                echo "<br/></div>";
            }
            pg_free_result($result);
            pg_close($dbconn);
        ?>
    </body>
</html>            