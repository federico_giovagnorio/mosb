<?php
    session_start();
?>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width-device-width" initial-scale="1">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="css/mosb.css"/>
        <script src="/css/bootstrap.min.js"></script>
    </head>
    
    <body class="text-center">
        <div class="navbar"></div>
        <?php
            $dbconn = pg_connect("host=localhost
                                port=5432
                                dbname=mosb
                                user=postgres
                                password=password")
                                or die ("Could not connect: " . pg_last_error());
            if (!(isset($_POST['inviaProgetto']))){
                header("Location: ../index.html");
            }
            else{
                
                $username=$_SESSION['username'];
                if ($username == "")
                    header("Location: ../index.html");
                $email=$_POST['emailProgetto'];
                $titolo=$_POST['titoloProgetto'];
                $sezione=$_POST['sezioneProgetto'];
                $descrizione=$_POST['descrizioneProgetto'];
                $check_gpl=$_POST['gplProgetto'];
                if ($check_gpl == "YES")
                    $gpl = 'true';
                else
                    $gpl = 'false';
                $data = date("Y-m-d");
                
                $query1="INSERT INTO progetto values ($1, $2, $3, $4, $5, $6, $7)";
                $ret1 = pg_query_params($dbconn, $query1, array($username, $email, $titolo, $sezione, $descrizione, $gpl, $data));
                if ($ret1) {
                    echo "<h1> Inserimento riuscito !</h1>";
                    echo "<div class='Intestazione'> Puoi tornare nella <a href='index.html'>HOME</a>, oppure puoi controllare i <a href='listaProgettoPersonale.html'>TUOI PROGETTI</a> </div>";
                    
                }
                else
                        echo "<h1> Inserimento NON riuscito, prova a cambiare titolo</h1>";
                }
        ?>
    </body>
</html>